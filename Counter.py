import timeit
import datetime

class Counter:
    """Gives an increasing sequence of integer values.
    
        Attributes: 
            'report' ... if True then a message will appear on the terminal each
                time the value of the counter is increased and reaches a value
                given by an integer multiple of the attribute 'period'
            'period' ... see the description of the attribute 'report'
            'time' ... the time of the creation of the instance of this class
    """

    def __init__(self, counter = -1):
        """Initialization.

            Parameters:
                'counter' ... specifies the initial value of the counter
        """
        self.counter = counter
        self.report = False
        self.period = 10000
        self.time = timeit.default_timer()

    def getNew(self):
        """The value of the counter is increased by 1 and this new value is returned."""
        self.counter = self.counter + 1
        if self.report and self.counter % self.period == 0:
            newTime = timeit.default_timer()
            print datetime.datetime.today(),
            print "Counter has reached", self.counter, "in", (newTime - self.time), "seconds."
            self.time = newTime
        return self.counter

    def getCurrent(self):
        """Returns the last counter in the sequence. The value of the counter is not increased."""
        return self.counter

