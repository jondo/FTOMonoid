
# This Python source code file contains several functions that help to write
# tables of string and integer values to a terminal output (e.g. in Linux).

def getIntegerLength(number):
    """Computes the length of the given integer value when it is converted to
        the string.
    If it is a positive integer value then the method returns the number of
        digits.
    If it is a negative integer value then the method returns the number of
        digits plus one (because of the negative sign character).
    """
    if number == 0:
        return 1

    result = 0

    if number < 0:
        result += 1
        number = - number

    while number > 0:
        result += 1
        number /= 10

    return result


def alignInteger(number, cellSize):
    """Converts the given integer value to text.
    The length of the text is given by the parameter 'cellSize'.
    If the length of the integer is less than 'cellSize' then spaces are added in
        front if the returned text in order to match the length specified in
        'cellSize'.
    This function is supposed to be used when writing tables of integer values
        to the text format (e.g. to a terminal output in Linux)."""
    numLength = getIntegerLength(number)

    result = str(number)

    if numLength < cellSize:
        for i in range(cellSize - numLength):
            result = " " + result

    return result

def alignText(text, cellSize):
    """Adds space characters to the beginning of the string value specified in
        the parameter text such that its length is equal to the integer value
        specified in the parameter 'cellSize'.
    This function is supposed to be used when writing tables of values to the
        text format (e.g. to a terminal output in Linux)."""
    textLength = len(text)

    result = text

    if textLength < cellSize:
        for i in range(cellSize - textLength):
            result = " " + result

    return result

