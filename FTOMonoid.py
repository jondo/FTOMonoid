import copy
import os

from Error import *
from AlignText import *
from Counter import Counter
from FiniteChain import FiniteChain
from FTOMonoidMulTable import FTOMonoidMulTable
from FTOMonoidLevelEquivalence import FTOMonoidLevelEquivalence

class FTOMonoid(FiniteChain):
    """Finite, totally ordered monoid (tomonoid).

        Attributes:
            'counter' ... instance of Counter

            'serialNumber' ... serial number of this tomonoid

            'parent' ... reference to the tomonoid from which this one has been
                created as its co-extension
            'child' ... reference to the first co-extension of this tomonoid
            'sibling' ... reference to the next co-extension of the parent of this tomonoid
                
            'tab' ... instance of FTOMonoidMulTable
            'leq' ... instance of FTOMonoidLevelEquivalence

            (for the list of tomonoids see 'FNTOMonoidList', indices of the
                tomonoids are supposed to match with their serial numbers)
            (for the rest of attributes see 'FiniteChain')
    """

    def __init__(self, counter = None, bottom=0, top=0):
        """Initialization.

            Parameters:
                'counter' ... instance of Counter
                'top' ... the maximal value of the tomonoid
                'bottom' ... the minimal value of the tomonoid
        """
        FiniteChain.__init__(self, bottom, top)

        self.counter = counter

        if self.counter == None:
            self.serialNumber = None
        else:
            self.serialNumber = self.counter.getNew()

        self.parent = None
        self.child = None
        self.sibling = None

        self.tab = FTOMonoidMulTable(bottom, top)
        self.leq = FTOMonoidLevelEquivalence(bottom, top)

    # -------------------------------------------------------------------------
    #
    #   Encapsulation
    #
    # -------------------------------------------------------------------------

    def setSerialNumber(self, serialNumber):
        """Sets the value of the attribute 'serialNumber'."""
        self.serialNumber = serialNumber

    def getSerialNumber(self):
        """Returns the value of the attribute 'serialNumber'."""
        return self.serialNumber

    def setParent(self, parent):
        """Sets the value of the attribute 'parent'."""
        self.parent = parent

    def getParent(self):
        """Returns the value of the attribute 'parent'."""
        return self.parent

    def setChild(self, child):
        """Sets the value of the attribute 'child'."""
        self.child = child

    def getChild(self):
        """Returns the value of the attribute 'child'."""
        return self.child

    def setSibling(self, sibling):
        """Sets the value of the attribute 'sibling'."""
        self.sibling = sibling

    def getSibling(self):
        """Returns the value of the attribute 'sibling'."""
        return self.sibling

    # -------------------------------------------------------------------------
    #
    #   Methods to clone the instance
    #
    # -------------------------------------------------------------------------

    def getCopy(self):
        """Returns the (deep) copy of the object."""
        new = FTOMonoid(self.counter)
        new.createAsCopy(self)
        return new

    # -------------------------------------------------------------------------
    #
    #   "create as" methods
    #
    # -------------------------------------------------------------------------

    def createAsCopy(self, orig):
        """Creates this instance of the class 'FTOMonoid' as a copy of 'orig'.

            Parameters:
                'orig' ... an instance of the type FTOMonoid
        """
        self.setDesignatedValues(orig.bottom, orig.top)
        self.tab = orig.tab.getCopy()
        self.leq = orig.leq.getCopy()

        self.parent = orig.getParent()
        self.child = None
        self.sibling = None

    def createAsUndefined(self):
        """Creates this tomonoid as undefined.

            Only the pairs, whose monoid multiplication value is given by the
                theory, are defined.
            These are the pairs with unit coordinate and the pairs with a
                coordinate equal to 'bottom' or 'top' and contained, respectively,
                in the negative or positive cone.
            The rest of the pairs is "undefined".
        """
        self.leq.createUnitPairs()
        self.tab.setValuesOnUnitAxes()
        self.leq.createBottomPairs()
        self.leq.createTopPairs()
        self.tab.setValuesOnBottom()
        self.tab.setValuesOnTop()

        # negative part
        for i in range(self.bottom + 1, 0, 1):
            for j in range(self.bottom + 1, 0, 1):
                key = self.leq.addPairToNewUndefinedClass((i,j))
                self.tab.setValue((i,j), key)
        # positive part
        for i in range(1, self.top, 1):
            for j in range(1, self.top, 1):
                key = self.leq.addPairToNewUndefinedClass((i,j))
                self.tab.setValue((i,j), key)
        # negative-positive  part
        for i in range(self.bottom, 0, 1):
            for j in range(1, self.top + 1, 1):
                key = self.leq.addPairToNewUndefinedClass((i,j))
                self.tab.setValue((i,j), key)
        # positive-negative part
        for i in range(1, self.top + 1, 1):
            for j in range(self.bottom, 0, 1):
                key = self.leq.addPairToNewUndefinedClass((i,j))
                self.tab.setValue((i,j), key)

    def createAsTrivial(self):
        self.setDesignatedValues(0, 0)
        self.tab.createAsTrivial()
        self.leq.createAsTrivial()

    def createFromTable(self, table):
        self.tab.createFromTable(table)
        self.leq.createFromTable(table)
        self.setDesignatedValues(self.tab.bottom, self.tab.top)
        
    # -------------------------------------------------------------------------
    #
    #   methods to define or change the level set equivalence
    #
    # -------------------------------------------------------------------------

    def relatePairs(self, pair1, pair2):
        """Relates the two given pairs.

            Parameters:
                pair1, pair2 ... two pairs (tuples) of integers (tomonoid
                    values)
        """
        lvlSetKey1 = self.getValue(pair1)
        lvlSetKey2 = self.getValue(pair2)

        lvlSetKey = self.leq.mergeLvlSets(lvlSetKey1, lvlSetKey2)
        lvlSet = self.leq.getLvlSet(lvlSetKey)
        for pair in lvlSet:
            self.tab.setValue(pair, lvlSetKey)

    def getValue(self, pair):
        """Returns the value of the given pair.
            
            Parameters:
                pair ... pair (tuple) of indices which specifies the position
                    of the table element
            Returns:
                the value of an element in the multiplication table specified
                    by the indices
        """
        return self.tab.getValue(pair)

    def setValue(self, pair, value):
        """Sets the value of the given pair.

            The change is made both in the multiplication table and in the
                level set equivalence.
            
            Parameters:
                pair ... pair (tuple) of indices which specifies the position
                    of the table element
                value ... the value to which the table element is to be set
        """
        if self.tab.getValue(pair) != value:
            self.relatePairs(pair, (0,value))
            self.tab.setValue(pair, value)

    # ----------------------------------------------------------------------
    #
    #   Export
    #
    # ----------------------------------------------------------------------

    # ----------------------------------------------------------------------
    # exporting to text format
    # ----------------------------------------------------------------------

    def exportParentToText(self):
        if self.parent == None:
            return "None"
        else:
            return str(self.parent.getSerialNumber())

    def exportChildToText(self):
        if self.child == None:
            return "None"
        else:
            return str(self.child.getSerialNumber())

    def exportSiblingToText(self):
        if self.sibling == None:
            return "None"
        else:
            return str(self.sibling.getSerialNumber())

    def exportChildrenToText(self):
        if self.child == None:
            return "None"
        else:
            text = ""
            child = self.getChild()
            text += str(child.getSerialNumber())
            if child.getSibling() != None:
                text += ", " + child.exportSiblingsToText()
            return text

    def exportSiblingsToText(self):
        if self.sibling == None:
            return "None"
        else:
            text = ""
            sibling = self.getSibling()
            first = True
            while sibling != None:
                if first:
                    first = False
                else:
                    text += ", "
                text += str(sibling.getSerialNumber())
                sibling = sibling.getSibling()
            return text

    def exportToText(self):
        """Exports the values of this object to text.
            
            Returns:
                string containing text representation of this object; it can be
                    printed by the 'print' command
        """
        text = ""
        text += "Finite tomonoid"
        if self.serialNumber != None:
            text += ", no. " + str(self.serialNumber)
        text += "\n"
        text += self.tab.exportMulTableToText()
        return text

    def exportFamilyRelationsToText(self):
        """Exports the values of the attributes 'parent', 'child', and 'sibling' to text.
            
            Returns:
                string containing text representation of this object; it can be
                    printed by the 'print' command
        """
        text = ""
        text += "serial:   " + str(self.getSerialNumber())
        text += "\n"
        text += "parent:   " + self.exportParentToText()
        text += "\n"
        text += "child:    " + self.exportChildToText()
        text += "\n"
        text += "sibling:  " + self.exportSiblingToText()
        text += "\n"
        text += "children: " + self.exportChildrenToText()
        text += "\n"
        text += "siblings: " + self.exportSiblingsToText()
        text += "\n"
        return text

    # ----------------------------------------------------------------------
    # exporting to TeX format
    # ----------------------------------------------------------------------

    def exportMulTableToTeX(self):
        """Exports the multiplication table to TeX & TikZ.

            Exports the multiplication table to a TeX & TikZ source code which
                can be included to a TeX document.

            Returns:
                string containing the TeX & TikZ source code
        """
        return self.tab.exportToTeX()

    # ----------------------------------------------------------------------
    #
    #   Saving to a file
    #
    # ----------------------------------------------------------------------

    def getTeXFileName(self):
        fileName = ""
        fileName += "fin_tomonoid"
        fileName += "_size_" + str(self.getSize())
        fileName += "_no_" + str(self.getSerialNumber())
        fileName += ".tex"
        return fileName

    def getTeXHead(self):
        text = ""
        text += "\\documentclass{article}\n"
        text += "\\usepackage{tikz}\n"
        text += "\\usepackage{ftom}\n"
        text += "\\begin{document}\n"
        return text

    def getTeXTail(self):
        text = ""
        text += "\\end{document}\n"
        return text

    def getTeXFilePath(self, directory = None, fileName = None):
        if directory == None:
            directory = "./tex_output/"
        if not os.path.isdir(directory):
            print "Creating directory", directory
            os.makedirs(directory)

        if fileName == None:
            fileName = self.getTeXFileName()

        return directory + fileName

    def saveToTeXFile(self, directory = None, fileName = None):

        fileName = self.getTeXFilePath()

        out = open(fileName, "w")

        out.write(self.getTeXHead())
        out.write(self.exportMulTableToTeX())
        out.write(self.getTeXTail())

        out.close()
        print "File", fileName, "written."

    # ----------------------------------------------------------------------
    #
    #   Show
    #
    # ----------------------------------------------------------------------

    def show(self):
        self.tab.show()
        self.showDesignatedValues()
        self.leq.show()

    def showLevelSets(self):
        self.leq.show()

    def showAlgebraicProperties(self):
        print self.tab.exportAlgebraicPropertiesToText()

    def showFamilyRelations(self):
        print self.exportFamilyRelationsToText()

## **************************************************************************
##
##   Run
##
## **************************************************************************
#
#def testFTOMonoid_1():
#    table= [['-2','-1',' 2',' 2',' 2'],
#            ['-2','-1',' 1',' 1',' 2'],
#            ['-2','-1',' 0',' 1',' 2'],
#            ['-2','-2','-1','-1','-1'],
#            ['-2','-2','-2','-2','-2']]
#    ftom = FTOMonoid()
#    ftom.createFromTable(table)
#    ftom.show()
#
#def testFTOMonoid_relatePairs():
#    print "***********************************************************************"
#    ftom = FTOMonoid(-2,2)
#    ftom.createAsUndefined()
#    ftom.show()
#    print "****    relating pairs    *********************************************"
#    ftom.relatePairs((1,-1),(1,1))
#    ftom.show()
#    #print "****    relating pairs    *********************************************"
#    #ftom.relatePairs((1,0),(2,0))
#    #ftom.show()
#
#def testFTOMonoid_trivial():
#    print "***********************************************************************"
#    ftom = FTOMonoid()
#    ftom.createAsTrivial()
#    ftom.show()
#
##testFTOMonoid_relatePairs()
#testFTOMonoid_trivial()
