import copy

from Error import *
from AlignText import *
from FiniteChain import FiniteChain

class FTOMonoidMulTable(FiniteChain):
    """Multiplication table of a finite, totally ordered monoid (tomonoid).

        It contains the definition of a tomonoid multiplication in a form of a
            table, i.e., a list of lists.
        It has methods to change the values in the table, to get a value from the
            table, and to export the table to various formats (text and TeX).

        Attributes:
            'mulTable' ... list of lists that contains the values of the
                    multiplication table
                The size of the table is given by the inherited attribute
                    'size'.
                If a value is out of the range of the chain values, it means
                    that the value of this element of the table is "undefined".
                Every element of the table is given by two indices which may
                    attains the integer values varying from 'bottom' to 'top'.
                Writing to the table, as well as reading the table values,
                    is suppossed to be done exclusively using the methods
                    'getValue' and 'setValue'.
            'allPairsDefined' ... True if all the pairs in the table have some
                value (even if the value is out of the range of the tomonoid
                and thus represents an "undefined" pair of the table)
    """

    def __init__(self, bottom=0, top=0):
        """Sets designated values and creates an empty table.

            Creates an empty table (i.e. with undefined values) of the size
                given by the parameters 'bottom' and 'top'.

            Parameters:
                'bottom' ... the minimal value of the finite tomonoid
                'top' ... the maximal value of the finite tomonoid
        """
        FiniteChain.__init__(self, bottom, top)
        self.createEmptyMulTable()
        self.allPairsDefined = False

    # -------------------------------------------------------------------------
    #
    #   Methods that initialize the object
    #
    # -------------------------------------------------------------------------

    def createEmptyMulTable(self):
        """Creates the data structure for the multiplication table.

            The data structure is a list of 'size' lists of 'size' integer
                values.
            See the class 'FiniteChain' for the description of the attribute
                'size'.
            The values in the table are all set to None.
        """
        self.mulTable = self.size * [[]]
        for i in range(self.size):
            self.mulTable[i] = self.size * [None]

    # -------------------------------------------------------------------------
    #
    #   Methods to get and set values of the multiplication table
    #
    # -------------------------------------------------------------------------

    def getValue(self, pair):
        """Returns the value of an element in the multiplication table.
            
            Parameters:
                pair ... pair (tuple) of indices which specifies the position
                    of the table element
            Returns:
                the value of an element in the multiplication table specified
                    by the indices
        """
        i = pair[0]
        j = pair[1]
        if i < self.bottom or i > self.top or j < self.bottom or j > self.top:
            raise Error, "Indices " + str(pair) + " out of bounds!"
        return self.mulTable[i - self.bottom][j - self.bottom]

    def setValue(self, pair, value):
        """Sets the value of an element in the multiplication table.
            
            Parameters:
                pair ... pair (tuple) of indices which specifies the position
                    of the table element
                value ... the value to which the table element is to be set
        """
        i = pair[0]
        j = pair[1]
        if i < self.bottom or i > self.top or j < self.bottom or j > self.top:
            raise Error, "Indices " + str(pair) + " out of bounds!"
        #if value < self.bottom or value > self.top:
        #    raise Error, "Value " + str(value) + " out of bounds!"
        self.mulTable[i - self.bottom][j - self.bottom] = value

    def setValuesOnUnitAxes(self):
        """Sets the values along the axes given by the neutral (unit) element.

            As '0' is understood as the neutral element, the value of every
                table element on the indices '(i, 0)' or '(0, i)' is set to the
                value 'i'.
        """
        for i in range(self.bottom, self.top + 1):
            self.setValue((i,0), i)
            self.setValue((0,i), i)

    def setValuesOnBottom(self):
        """Creates pairs with 'bottom' coordinate."""
        if self.bottom < 0:
            self.setValue((self.bottom,self.bottom), self.bottom)
            for i in range(self.bottom + 1, 0, 1):
                self.setValue((self.bottom,i), self.bottom)
                self.setValue((i,self.bottom), self.bottom)

    def setValuesOnTop(self):
        """Creates pairs with 'top' coordinate."""
        if self.top > 0:
            self.setValue((self.top,self.top), self.top)
            for i in range(self.top - 1, 0, -1):
                self.setValue((self.top,i), self.top)
                self.setValue((i,self.top), self.top)

    # -------------------------------------------------------------------------
    #
    #   Methods to clone multiplication table and to compare two multiplication
    #   tables
    #
    # -------------------------------------------------------------------------

    def getCopy(self):
        """Returns the (deep) copy of the object."""
        new = FTOMonoidMulTable()
        new.createAsCopy(self)
        return new

    def isEqual(self, ftom):
        """Compares two instances of the class FTOMonoidMulTable.

            Compares whether this multiplication table is equal to the
                multiplication table given by the parameter 'ftom'.
            To be equal means to have the same 'bottom', 'top', and to have the
                same values on the same positions in the table.
            Returns:
                True or False
        """
        if self.bottom != ftom.bottom or self.top != ftom.top:
            return False
        for i in range(self.bottom, self.top + 1):
            for j in range(self.bottom, self.top + 1):
                if self.getValue((i, j)) != ftom.getValue((i, j)):
                    return False
        return True

    # -------------------------------------------------------------------------
    #
    #   "create as" methods
    #   -------------------
    #
    #   These methods serve to fill all the values in the multiplication
    #   table to obtain various kinds of special types of tomonoids.
    #
    # -------------------------------------------------------------------------

    def createAsCopy(self, orig):
        """Creates this multiplication table as the copy of 'orig'.

            Parameters:
                'orig' ... an instance of the type FTOMonoidMulTable
        """
        self.setDesignatedValues(orig.bottom, orig.top)
        self.mulTable = copy.deepcopy(orig.mulTable)
        self.allPairsDefined = orig.allPairsDefined

    def createAsMinimalDrastic(self):
        """Creates this multiplication table such that both the positive and the negative
                part are drastic and the side part is minimal.
        """
        self.setValuesOnUnitAxes()
        for i in range(self.bottom, 0):
            for j in range(self.bottom, 0):
                self.setValue((i,j),self.bottom)
        for i in range(1, self.top + 1):
            for j in range(1, self.top + 1):
                self.setValue((i,j),self.top)
        for i in range(self.bottom, 0):
            for j in range(1, self.top + 1):
                self.setValue((i,j),i)
                self.setValue((j,i),i)
        self.allPairsDefined = True

    def createAsMaximalDrastic(self):
        """Creates this multiplication table such that both the positive and the negative
                part are drastic and the side part is maximal.
        """
        self.setValuesOnUnitAxes()
        for i in range(self.bottom, 0):
            for j in range(self.bottom, 0):
                self.setValue((i,j),self.bottom)
        for i in range(1, self.top + 1):
            for j in range(1, self.top + 1):
                self.setValue((i,j),self.top)
        for i in range(self.bottom, 0):
            for j in range(1, self.top + 1):
                self.setValue((i,j),j)
                self.setValue((j,i),j)
        self.allPairsDefined = True

    def createAsMinimal(self):
        """Creates this multiplication table as the minimal one, i.e, the monoidal
                multiplication on the positive part is the maximum, on the negative
                part is equal to 'bottom', and on the side part it is the minimum.
        """
        self.setValuesOnUnitAxes()
        for i in range(self.bottom, 0):
            for j in range(self.bottom, 0):
                self.setValue((i,j),self.bottom)
        for i in range(1, self.top + 1):
            for j in range(1, self.top + 1):
                self.setValue((i,j),max(i,j))
        for i in range(self.bottom, 0):
            for j in range(1, self.top + 1):
                self.setValue((i,j),i)
                self.setValue((j,i),i)
        self.allPairsDefined = True

    def createAsMaximal(self):
        """Creates this multiplication table as the maximal one, i.e, the monoidal
                multiplication on the positive part is equal to 'top', on the
                negative part it is the minimum, and on the side part it is the
                maximum.
        """
        self.setValuesOnUnitAxes()
        for i in range(self.bottom, 0):
            for j in range(self.bottom, 0):
                self.setValue((i,j),min(i,j))
        for i in range(1, self.top + 1):
            for j in range(1, self.top + 1):
                self.setValue((i,j),self.top)

        for i in range(self.bottom, 0):
            for j in range(1, self.top + 1):
                self.setValue((i,j),j)
                self.setValue((j,i),j)
        self.allPairsDefined = True

    def createFromNegPos(self, neg, pos):
        """Creates this multiplication table from a given negative and positive
            multiplication tables.

            Given a negative multiplication table (i.e., the neutral element
                '0' is the greatest one) and a positive multiplication table (i.e.,
                the neutral element '0' is the least one) a new multiplication
                table, which negative and positive cone matches respectively with
                the given negative and positive multiplication table, is created.

            The values that do not belong neither to the negative cone nor to
                the positive cone remain undefined.

            Parameters:
                'neg' ... negative multiplication table
                'pos' ... positive multiplication table
        """
        self.setDesignatedValues(neg.bottom, pos.top)
        self.createEmptyMulTable()
        self.setValuesOnUnitAxes()
        for i in range(self.bottom, 0):
            for j in range(self.bottom, 0):
                value = neg.getValue((i,j))
                self.setValue((i,j),value)
        for i in range(1, self.top + 1):
            for j in range(1, self.top + 1):
                value = pos.getValue((i,j))
                self.setValue((i,j),value)
        self.allPairsDefined = False

    def createFromNegPosAsMinimal(self, neg, pos):
        """Creates this multiplication table from a given negative and positive
            multiplication tables.

            Given a negative multiplication table (i.e., the neutral element
                '0' is the greatest one) and a positive multiplication table (i.e.,
                the neutral element '0' is the least one) a new multiplication
                table, which negative and positive cone matches respectively with
                the given negative and positive multiplication table, is created.

            The values that do not belong neither to the negative cone nor to
                the positive cone are defined to be equal to the lower one of
                their two indices.

            Parameters:
                'neg' ... negative multiplication table
                'pos' ... positive multiplication table
        """
        self.createFromNegPos(neg, pos)
        for i in range(self.bottom, 0):
            for j in range(1, self.top + 1):
                self.setValue((i,j),i)
                self.setValue((j,i),i)
        self.allPairsDefined = True

    def createFromNegPosAsMaximal(self, neg, pos):
        """Creates this multiplication table from a given negative and positive
            multiplication tables.

            Given a negative multiplication table (i.e., the neutral element
                '0' is the greatest one) and a positive multiplication table (i.e.,
                the neutral element '0' is the least one) a new multiplication
                table, which negative and positive cone matches respectively with
                the given negative and positive multiplication table, is created.

            The values that do not belong neither to the negative cone nor to
                the positive cone are defined to be equal to the greater one of
                their two indices.

            Parameters:
                'neg' ... negative multiplication table
                'pos' ... positive multiplication table
        """
        self.createFromNegPos(neg, pos)
        for i in range(self.bottom, 0):
            for j in range(1, self.top + 1):
                self.setValue((i,j),j)
                self.setValue((j,i),j)
        self.allPairsDefined = True

    def createAsTrivial(self):
        """Creates this multiplication table such that it represents the trivial monoid.

            The trivial monoid is the monoid that contains the neutral element
                '0' only.
            Therefore the resulting table will have one row and one
                column---that is, one cell only.
            This single cell will contain the neutral element '0'.
        """
        self.setDesignatedValues(0, 0)
        self.createEmptyMulTable()
        self.setValue((0, 0), 0)
        self.allPairsDefined = True

    def createFromTable(self, table):
        """Creates this multiplication table according to the given table of values.
            
            Parameters:
                'table' ... list of n lists of n values from 'bottom', ..., '0', ..., 'top'

            Example:
                table= [['-2','-1',' 2',' 2',' 2'],
                        ['-2','-1',' 1',' 2',' 2'],
                        ['-2','-1',' 0',' 1',' 2'],
                        ['-2','-2','-1','-1','-1'],
                        ['-2','-2','-2','-2','-2']]
                leq = FTOMonoidLevelEquivalence()
                leq.createFromTable(table)
        """

        # size of the multiplication table
        self.size = len(table)

        # looking for the neutral element
        unit = None
        for x in range(self.size):
            if int(table[self.size - x - 1][x]) == 0:
                unit = x
                break
        if unit == None:
            raise Error, "Unit element not found in the given table!"

        # setting the designated values
        self.setDesignatedValues(-unit, self.size - unit - 1)

        # creating the multiplication table
        self.createEmptyMulTable()

        # filling the multiplication table
        for x in range(self.size):
            for y in range(self.size):
                value = int(table[y][self.size - 1 - x])
                i = (self.size - y - 1) + self.bottom
                j = (self.size - x - 1) + self.bottom
                self.setValue((i,j), value)

        # now all the pairs are assigned to some value
        self.allPairsDefined = True

    # -------------------------------------------------------------------------
    #
    #   Methods to iterate through various tables
    #   -----------------------------------------
    #
    #   According to the current table a new table is generated such that all
    #   the possible tables are obtained in one period.
    #   All the tables created by this iteration preserve monotonicity of the
    #   monoidal operation.
    #
    # -------------------------------------------------------------------------

    # table must be created with 'createAsMinimal'
    # iteration sets elements of table to both negative and positive values ...
    # this can be simplified

    def getMaxBound(self, pair):
        """
            Returns: 
                the maximal value which the element, on the indices given by
                    the parameter 'pair', can attain
            Remark:
                The methods supposes that the elements on the higher indices
                    are already set.
        """
        i = pair[0]
        j = pair[1]
        if i < self.bottom or i > self.top or j < self.bottom or j > self.top:
            raise Error, "Indices " + str(pair) + " out of bounds!"
        #if i >= self.top:
        #    if j >= self.top:
        if i == self.top:
            if j == self.top:
                return self.top
            else:
                return self.getValue((i,j+1))
        else:
            if j == self.top:
                return self.getValue((i+1,j))
            else:
                return min(self.getValue((i+1,j)), self.getValue((i,j+1)))

    def getPosNegInitialPair(self):
        """
            Returns:
                the pair in the positive-negative part of the multiplication
                    table whose value is to be set as the first one;
                if the positive-negative part is too small (i.e., if the
                    tomonoid is negative or positive) then the 'None' value is
                    returned
        """
        if self.bottom == 0 or self.top == 0:
            return None
        else:
            return (1, self.bottom)

    def getPosNegNextPair(self, pair):
        """Iterates through the pairs in the positive-negative part of the
            multiplication table from (1,bottom) to (top,-1).

            Returns:
                the next pair in the iteration process
                and 'None' when the iteration is finished
        """
        if self.bottom == 0 or self.top == 0:
            return None
        else:
            i = pair[0]
            j = pair[1]
            i += 1
            if i > self.top:
                i = 1
                j += 1
                if j >= 0:
                    return None
        return (i,j)

    def iteratePosNeg(self):
        """Makes a new evaluation of the positive-negative part of the
            multiplication table.

            When the last evaluation is over (i.e. if the whole period if these
                evaluations has been already iterated) then False is returned,
                otherwise the method returns True.
            Returns:
                'True' if the iteration still may continue,
                'False' if the iteration is over
            Example:
                ftom = FTOMonoidMulTable(-3,2)
                ftom.createAsMinimal()
                ftom.show()
                while ftom.iteratePosNeg():
                    ftom.show()
        """
        pair = self.getPosNegInitialPair()

        while True:
            if pair == None:
                return False
            if self.getValue(pair) < self.getMaxBound(pair):
                break
            self.setValue(pair, min(pair))
            pair = self.getPosNegNextPair(pair)

        value = self.getValue(pair)
        self.setValue(pair, value + 1)
        return True

    def getNegPosInitialPair(self):
        """
            Returns:
                the pair in the negative-positive part of the multiplication
                    table whose value is to be set as the first one;
            if the negative-positive part is too small (i.e., if the tomonoid
                is negative or positive) then the 'None' value is returned.
        """
        if self.bottom == 0 or self.top == 0:
            return None
        else:
            return (self.bottom, 1)

    def getNegPosNextPair(self, pair):
        """Iterates through the pairs in the negative-positive part of the
            multiplication table from (bottom,1) to (-1,top).

            Returns:
                the next pair in the iteration process
                and 'None' when the iteration is finished
        """
        if self.bottom == 0 or self.top == 0:
            return None
        else:
            i = pair[0]
            j = pair[1]
            j += 1
            if j > self.top:
                j = 1
                i += 1
                if i >= 0:
                    return None
        return (i,j)

    def iterateNegPos(self):
        """Makes a new evaluation of the negative-positive part of the
            multiplication table.

            When the last evaluation is over (i.e. if the whole period if these
                evaluations has been already iterated) then False is returned,
                otherwise the method returns True.
            Returns:
                'True' if the iteration still may continue,
                'False' if the iteration is over
            Example:
                ftom = FTOMonoidMulTable(-3,2)
                ftom.createAsMinimal()
                ftom.show()
                while ftom.iterateNegPos():
                    ftom.show()
        """
        pair = self.getNegPosInitialPair()

        while True:
            if pair == None:
                return False
            if self.getValue(pair) < self.getMaxBound(pair):
                break
            self.setValue(pair, min(pair))
            pair = self.getNegPosNextPair(pair)

        value = self.getValue(pair)
        self.setValue(pair, value + 1)
        return True

    def iterateSidePart(self):
        """Makes a new evaluation of both the positive-negative and
            negative-positive part of the multiplication table.

            When the last evaluation is over (i.e. if the whole period if these
                evaluations has been already iterated) then False is returned,
                otherwise the method returns True.
            Returns:
                'True' if the iteration still may continue,
                'False' if the iteration is over
            Example:
                ftom = FTOMonoidMulTable(-3,2)
                ftom.createAsMinimal()
                ftom.show()
                while ftom.iterateSidePart():
                    ftom.show()
        """
        if not self.iterateNegPos():
            if not self.iteratePosNeg():
                return False
        return True

    # -------------------------------------------------------------------------
    #
    #   Algebraic tests
    #
    # -------------------------------------------------------------------------

    # -------------------------------------------------------------------------
    # associativity
    # -------------------------------------------------------------------------

    def isAssociativeOn(self, x, y, z):
        """Tests whether the given values satisfy the associativity equation.

            Returns:
                'True' ... if the values 'x', 'y', and 'z' comply with the
                    associativity equation, i.e., if (xy)z = z(yz)
                'False' ... otherwise
        """
        xy = self.getValue((x,y))
        yz = self.getValue((y,z))
        if self.getValue((x,yz)) == self.getValue((xy,z)):
            return True
        else:
            return False

    def isAssociative(self):
        """Tests whether this multiplication table is associative.

            Returns:
                'True' ... if the multiplication table represents an associative
                    structure with the neutral element '0'.
                'False' ... otherwise

            Remark:
                The test is not performed for the triplets 'x', 'y', 'z' where
                    one of the values is equal to '0'.
        """
        for x in range(self.bottom, self.top + 1):
            if x != 0:
                for y in range(self.bottom, self.top + 1):
                    if y != 0:
                        if x < 0 and y < 0:
                            for z in range(1, self.top + 1):
                                if not self.isAssociativeOn(x, y, z):
                                    return False
                        elif x > 0 and y > 0:
                            for z in range(self.bottom, 0):
                                if not self.isAssociativeOn(x, y, z):
                                    return False
                        else:
                            for z in range(self.bottom, self.top + 1):
                                if z != 0:
                                    if not self.isAssociativeOn(x, y, z):
                                        return False
        return True

    def isAssociativeEverywhere(self):
        """Tests whether this multiplication table is associative.

            Returns:
                'True' ... if the multiplication table represents an associative
                    structure with the neutral element '0'.
                'False' ... otherwise

            Remark:
                The test is performed for all the triplets 'x', 'y', 'z'
                    (confer with the method 'isAssociative').
        """
        for x in range(self.bottom, self.top + 1):
            for y in range(self.bottom, self.top + 1):
                for z in range(self.bottom, self.top + 1):
                    if not self.isAssociativeOn(x, y, z):
                        return False
        return True

    def getAssociativityTestReport(self):
        """Tests whether this multiplication table is associative.

            The triplets 'x', 'y', 'z' that do not comply with the
                associativity equation, i.e. such that (xy)z != z(yz), are stored
                to a list which is finally returned by the method.

            Returns:
                list of triplets (x, y, z) that do not comply with the
                    associativity equation

            Remark:
                If the multiplication table actually is associative then an
                    empty list is returned.
        """
        report = []
        for x in range(self.bottom, self.top + 1):
            for y in range(self.bottom, self.top + 1):
                for z in range(self.bottom, self.top + 1):
                    if not self.isAssociativeOn(x, y, z):
                        report.append((x, y, z))
        return report

    # -------------------------------------------------------------------------
    # monotonicity
    # -------------------------------------------------------------------------

    def isMonotoneOn(self, i, j):
        """Tests whether the given pair satisfies the monotonicity condition.

            Returns:
                'True' ... if the parameters 'i' and 'j' comply with the
                    monotonicity condition, i.e., if (i-1, j) <= (i,j) <= (i,j+1).
                'False' ... otherwise
        """
        if i > j:
            value = self.getValue((i, j))
            valueI = self.getValue((i-1, j))
            valueJ = self.getValue((i, j+1))
            result = value >= valueI and value <= valueJ
        elif i < j:
            value = self.getValue((i, j))
            valueI = self.getValue((i+1, j))
            valueJ = self.getValue((i, j-1))
            result = value <= valueI and value >= valueJ
        else:
            result = True
        return result

    def isMonotone(self):
        """Tests whether this multiplication table is monotone.

            Returns:
                'True' ... if the multiplication table is monotone
                'False' ... otherwise
        """
        for i in range(1, self.top + 1):
            for j in range(self.bottom, 0):
                if not self.isMonotoneOn(i, j) or not self.isMonotoneOn(j, i):
                    return False
        return True

    def isMonotoneEverywhere(self):
        """Tests whether this multiplication table is monotone.

            Returns:
                'True' ... if the multiplication table is monotone
                'False' ... otherwise
        """
        for i in range(self.bottom, self.top):
            for j in range(self.bottom, self.top):
                if not self.isMonotoneOn(i, j):
                    return False
        return True

    def getMonotonicityTestReport(self):
        """Tests whether this multiplication table is monotone.

            The pairs (i,j) that do not comply with the monotonicity condition
                (see method 'isMonotoneOn') are stored to a list which is finally
                returned by the method.

            Returns:
                list of pairs (i, j) that do not comply with the monotonicity
                    condition

            Remark:
                If the multiplication table is actually monotone then an empty
                    list is returned.
        """
        report = []
        for i in range(self.bottom, self.top):
            for j in range(self.bottom, self.top):
                if not self.isMonotoneOn(i, j):
                    report.append((i, j))
        return report

    # -------------------------------------------------------------------------
    # commutativity
    # -------------------------------------------------------------------------

    def isCommutativeOn(self, i, j):
        """Tests whether the given values satisfy the commutativity equation.

            Returns:
                'True' ... if the values 'i' and 'j' satisfy the commutativity
                    equation, i.e., if i*j = j*i.
                'False' ... otherwise
        """
        if self.getValue((i,j)) == self.getValue((j,i)):
            return True
        else:
            return False

    def isCommutative(self):
        """Tests whether this multiplication table is commutative.

            Returns:
                'True' ... if the multiplication table is commutative.
                'False' ... otherwise
        """
        for i in range(self.bottom, self.top):
            for j in range(self.bottom, self.top):
                if not self.isCommutativeOn(i, j):
                    return False
        return True

    def getCommutativityTestReport(self):
        """Tests whether this multiplication table is commutative.

            The pairs (i, j) that do not satisfy the commutativity equation
                (see method 'isCommutativeOn') are stored to a list which is finally
                returned by the method.

            Returns:
                list of pairs (i, j) that do not satisfy the commutativity equation

            Remark:
                If the multiplication table actually is commutative then an
                    empty list is returned.
        """
        report = []
        for i in range(self.bottom, self.top):
            for j in range(self.bottom, self.top):
                if not self.isCommutativeOn(i, j):
                    report.append((i, j))
        return report

    # -------------------------------------------------------------------------
    # idempotency
    # -------------------------------------------------------------------------

    def isIdempotentOn(self, i):
        """Tests whether the given value satisfies the idempotency condition.

            Returns:
                'True' ... if the parameter 'i' is an idempotent element of
                    the multiplication table, i.e., if i*i = i
                'False' ... otherwise
        """
        if self.getValue((i,i)) == i:
            return True
        else:
            return False

    def isPosIdempotent(self):
        """Tests whether all the positive elements of the tomonoid are
            idempotent.

            Returns:
                'True' ... if all the positive elements of the tomonoid are
                    idempotent
                'False' ... otherwise
        """
        for i in range(1, self.top, 1):
            if not self.isIdempotentOn(i):
                return False
        return True

    def isNegIdempotent(self):
        """Tests whether all the negative elements of the tomonoid are
            idempotent.

            Returns:
                'True' ... if all the negative elements of the tomonoid are
                    idempotent
                'False' ... otherwise
        """
        for i in range(-1, self.bottom, -1):
            if not self.isIdempotentOn(i):
                return False
        return True

    def isIdempotent(self):
        """Tests whether all the elements of the tomonoid are idempotent.

            Returns:
                'True' ... if all the elements of the tomonoid are
                    idempotent
                'False' ... otherwise
        """
        return self.isPosIdempotent() and self.isNegIdempotent()

    # -------------------------------------------------------------------------
    # local internality
    # -------------------------------------------------------------------------

    def isLocallyInternalOn(self, i, j):
        """Tests whether the given pair is locally internal.

            Returns:
                'True' ... if the pair (i,j) is locally internal, i.e., if i*j
                    is equal either to 'i' or to 'j'
                'False' ... otherwise
        """
        value = self.getValue((i, j))
        if value == min(i, j) or value == max(i, j):
            return True
        else:
            return False

    def isLocallyInternal(self):
        """Tests whether the multiplication table is locally internal.

            Returns:
                'True' ... if all the pairs in the negative-positive and
                    positive-negative part of the multiplication table are locally
                    internal
                'False' ... otherwise
        """
        for i in range(self.top, 0, -1):
            for j in range(self.bottom, 0):
                if not self.isLocallyInternalOn(i, j):
                    return False
        return True

    # -------------------------------------------------------------------------
    # stairs-like property
    # for all i in N: i*1 = i*top
    # -------------------------------------------------------------------------

    def isStairsLike(self):
        """Tests whether the multiplication table is "stairs-like".

            Returns:
                'True' ... if every the element i of the tomonoid, apart from
                    the neutral element 0, satisfy the property i * 1 = top
                'False' ... otherwise
        """
        if self.bottom == 0 or self.top == 0:
            return True
        for i in range(-1, self.bottom, -1):
            if self.getValue((i, 1)) != self.getValue((i, self.top)):
                return False
            if self.getValue((1, i)) != self.getValue((self.top, i)):
                return False
        return True

    # -------------------------------------------------------------------------
    # Archimedeanicity
    # -------------------------------------------------------------------------

    def isPosArchimedean(self):
        """Tests whether the positive part of the multiplication table is Archimedean.
        
            Returns:
                'True' ... if the positive part of the tomonoid is Archimedean,
                    i.e., if i*i > i holds for every element i of the positive part
                    of the tomonoid except for the neutral element 0 and the top
                    element
                'False' ... otherwise
        """
        if self.top == 0:
            return True
        for i in range(1, self.top, 1):
            if self.isIdempotentOn(i):
                return False
        return True

    def isNegArchimedean(self):
        """Tests whether the negative part of the multiplication table is Archimedean.
        
            Returns:
                'True' ... if the negative part of the tomonoid is Archimedean,
                    i.e., if i*i > i holds for every element i of the negative part
                    of the tomonoid except for the neutral element 0 and the bottom
                    element                
                'False' ... otherwise
        """
        if self.bottom == 0:
            return True
        for i in range(-1, self.bottom, -1):
            if self.isIdempotentOn(i):
                return False
        return True

    def isArchimedean(self):
        """Tests whether the multiplication table is Archimedean.
        
            Returns:
                'True' ... if both the positive part and the negative part of
                    the tomonoid are Archimedean; see methods 'isPosArchimedean'
                    and 'isNegArchimedean'               
                'False' ... otherwise
        """
        return self.isPosArchimedean() and self.isNegArchimedean()

    # -------------------------------------------------------------------------
    # drasticity
    # -------------------------------------------------------------------------

    def isPosDrastic(self):
        """Tests whether the positive part of the multiplication table is drastic.
        
            Returns:
                'True' ... if the positive part of the tomonoid is drastic,
                    i.e., if i*j = 'top' holds for every two elements i and j of
                    the positive part of the tomonoid except for the neutral
                    element 0
                'False' ... otherwise
        """
        if self.top == 0:
            return True
        else:
            return self.getValue((1,1)) == self.top

    def isNegDrastic(self):
        """Tests whether the negative part of the multiplication table is drastic.
        
            Returns:
                'True' ... if the negative part of the tomonoid is drastic,
                    i.e., if i*j = 'bottom' holds for every two elements i and j of
                    the negative part of the tomonoid except for the neutral
                    element 0
                'False' ... otherwise
        """
        if self.bottom == 0:
            return True
        else:
            return self.getValue((-1,-1)) == self.bottom

    def isDrastic(self):
        """Tests whether the multiplication table is drastic.
        
            Returns:
                'True' ... if both the positive part and the negative part of
                    the tomonoid are drastic; see methods 'isPosDrastic' and
                    'isNegDrastic'
                'False' ... otherwise
        """
        return self.isPosDrastic() and self.isNegDrastic()

    # ----------------------------------------------------------------------
    #
    #   Export
    #
    # ----------------------------------------------------------------------

    # ----------------------------------------------------------------------
    # exporting to text format
    # ----------------------------------------------------------------------

    def exportMulTableToText(self):
        """Exports the multiplication table to text.

            Exports the multiplication table to the text format which is
                suitable to be printed to a terminal or to be saved to a text file.

            Returns:
                string containing the formated table; can be printed by the
                    'print' command
        """
        text = ""
        text += alignText("i\j | ", self.cellSize + 3)
        for j in range(self.bottom, self.top + 1):
            text += self.exportValueToText(j)
            text += " "
        text += "\n"
        for i in range(self.cellSize + 1):
            text += "-"
        text += "+-"
        for i in range(self.size * (self.cellSize + 1)):
            text += "-"
        text += "\n"
        firstRun = True
        for i in range(self.top, self.bottom - 1, -1):
            if firstRun:
                firstRun = False
            else:
                text += "\n"
            text += self.exportValueToText(i)
            text += " | "
            for j in range(self.bottom, self.top + 1):
                value = self.getValue((i,j))
                if value == None:
                    text += alignText("N", self.cellSize)
                else:
                    text += self.exportValueToText(value)
                text += " "
        return text

    def exportAlgebraicPropertiesToText(self):
        """Exports algebraic properties of the multiplication table to text.

            Returns:
                string containing text representation of this object; it can be
                    printed by the 'print' command
        """
        text = ""
        text += "Associative:     " + str(self.isAssociative()) + "  " + str(self.getAssociativityTestReport()) + "\n"
        text += "Commutative:     " + str(self.isCommutative()) + "  " + str(self.getCommutativityTestReport()) + "\n"
        text += "Monotone:        " + str(self.isMonotone())    + "  " + str(self.getMonotonicityTestReport())  + "\n"
        text += "LocallyInternal: " + str(self.isLocallyInternal())                                             + "\n"
        text += "StairsLike:      " + str(self.isStairsLike())                                                  + "\n"
        text += "Pos Idempotent:  " + str(self.isPosIdempotent())                                               + "\n"
        text += "Neg Idempotent:  " + str(self.isNegIdempotent())                                               + "\n"
        text += "Pos Archimedean: " + str(self.isPosArchimedean())                                              + "\n"
        text += "Neg Archimedean: " + str(self.isNegArchimedean())                                              + "\n"
        text += "Pos Drastic:     " + str(self.isPosDrastic())                                                  + "\n"
        text += "Neg Drastic:     " + str(self.isNegDrastic())
        return text

    def exportToText(self):
        """Exports the values of this object to text.
            
            Returns:
                string containing text representation of this object; it can be
                    printed by the 'print' command
        """
        text = ""
        text += self.exportMulTableToText()
        #text += self.exportAlgebraicPropertiesToText()
        return text

    # ----------------------------------------------------------------------
    # exporting to TeX format
    # ----------------------------------------------------------------------

    def exportToTeX(self):
        """Exports the multiplication table to TeX & TikZ.

            Exports the multiplication table to a TeX & TikZ source code which
                can be included to a TeX document.

            Returns:
                string containing the TeX & TikZ source code
        """
        posNeutral = -self.bottom

        text = ""
        text += "\\begin{tikzpicture}[scale=1.0]\n"
        text += "    \\begin{scope}\n"
        text += "        \\edef\\tomtab{\n"

        iFirst = True
        for i in range(self.top, self.bottom - 1, -1):
            if iFirst:
                iFirst = False
            else:
                text += ",\n"
            text += "            {"
            jFirst = True
            for j in range(self.bottom, self.top + 1):
                if jFirst:
                    jFirst = False
                else:
                    text += ", "
                value = self.getValue((i,j))
                if value == None:
                    text += alignText("N", self.cellSize)
                else:
                    text += self.exportValueToText(value)
            text += "}"

        text += "}\n"
        text += "        \\tomTable[](\\tomtab," + str(posNeutral) + ")\n"
        text += "    \\end{scope}\n"
        text += "\\end{tikzpicture}\n"
        return text

    # ----------------------------------------------------------------------
    #
    #   Show
    #
    # ----------------------------------------------------------------------

    def show(self):
        """Prints the values of this object to the terminal."""
        print self.exportToText()


## **************************************************************************
##
##   Run
##
## **************************************************************************
#
#def testFTOMonoidMulTable_1():
#    table= [['-5','-4','-3','-2','-1',' 2',' 2',' 2'],
#            ['-5','-4','-3','-2','-1',' 1',' 2',' 2'],
#            ['-5','-4','-3','-2','-1',' 0',' 1',' 2'],
#            ['-5','-5','-5','-5','-5','-1','-1','-1'],
#            ['-5','-5','-5','-5','-5','-2','-2','-2'],
#            ['-5','-5','-5','-5','-5','-3','-3','-3'],
#            ['-5','-5','-5','-5','-5','-4','-4','-4'],
#            ['-5','-5','-5','-5','-5','-5','-5','-5']]
#    ftom = FTOMonoidMulTable()
#    ftom.createFromTable(table)
#    ftom.show()
#    print ftom.exportAlgebraicPropertiesToText()
#
#def testFTOMonoidMulTable_2():
#    table= [['-2','-1',' 2',' 2',' 2'],
#            ['-2','-1',' 1',' 1',' 2'],
#            ['-2','-1',' 0',' 1',' 2'],
#            ['-2','-2','-1','-1','-1'],
#            ['-2','-2','-2','-2','-2']]
#    ftom = FTOMonoidMulTable()
#    ftom.createFromTable(table)
#    ftom.show()
#    ftom.iterateSidePart()
#    ftom.show()
#
#def testFTOMonoidMulTable_3():
#    ftom = FTOMonoidMulTable(-3,2)
#    ftom.createAsMinimal()
#    counter = 0
#    print counter
#    ftom.show()
#    while ftom.iteratePosNeg():
#        counter += 1
#        print counter
#        ftom.show()
#
#testFTOMonoidMulTable_2()
#
#
#
