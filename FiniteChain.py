import copy

from Error import Error
from AlignText import *

class FiniteChain:
    """Finite set of integers that contains 0.

        This class stores some important informations about a finite chain
            (totally ordered set) of integers that contains 0.

        Attributes:
            'top' ... the maximal value
            'coatom' ... value of the second greates element
            'atom' ... value of the second least element
            'bottom' ... the minimal value
            'size' ... number of the elements in the chain
            'cellSize' ... this attribute serves to export the table to a
                terminal; it denotes the size of the table cell in the text
                format
    """

    def __init__(self, bottom=0, top=0):
        self.setDesignatedValues(bottom, top)

    def setDesignatedValues(self, bottom, top):
        """Sets the values of the object attributes.

            Sets the values of 'bottom', 'top', 'size', 'atom', 'coatom', and
                'cellSize'.
        """
        self.bottom = bottom
        self.top = top
        self.size = self.top - self.bottom + 1
        if self.size <= 1:
            self.atom = None
            self.coatom = None
        else:
            self.atom = self.bottom + 1
            self.coatom = self.top - 1
        self.computeCellSize()

    def computeCellSize(self):
        """Computes the attribute 'cellSize'."""
        self.cellSize = max(getIntegerLength(self.bottom), getIntegerLength(self.top), 2) + 1

    # -------------------------------------------------------------------------
    #
    #   Encapsulation
    #
    # -------------------------------------------------------------------------

    def setTop(self, top):
        """Sets the value of the attribute 'top'."""
        self.top = top

    def getTop(self):
        """Returns the value of the attribute 'top'."""
        return self.top

    def setCoatom(self, coatom):
        """Sets the value of the attribute 'coatom'."""
        self.coatom = coatom

    def getCoatom(self):
        """Returns the value of the attribute 'coatom'."""
        return self.coatom

    def setAtom(self, atom):
        """Sets the value of the attribute 'atom'."""
        self.atom = atom

    def getAtom(self):
        """Returns the value of the attribute 'atom'."""
        return self.atom

    def setBottom(self, bottom):
        """Sets the value of the attribute 'bottom'."""
        self.bottom = bottom

    def getBottom(self):
        """Returns the value of the attribute 'bottom'."""
        return self.bottom

    def setSize(self, size):
        """Sets the value of the attribute 'size'."""
        self.size = size

    def getSize(self):
        """Returns the value of the attribute 'size'."""
        return self.size

    # ----------------------------------------------------------------------
    #
    #   Export
    #
    # ----------------------------------------------------------------------

    # ----------------------------------------------------------------------
    # exporting to text format
    # ----------------------------------------------------------------------

    def exportValueToText(self, value):
        """Converts an integer value given to string whose length is given by
            the attribute 'cellSize'."""
        return alignInteger(value, self.cellSize)

    def exportDesignatedValuesToText(self):
        """Exports the values of this object to text format.

            Returns:
                string containing text representation of this object; it can be
                    printed by the 'print' command
        """
        text = ""
        text += "size =     " + self.exportValueToText(self.size)     + "\n"
        text += "top =      " + self.exportValueToText(self.top)      + "\n"
        text += "coatom =   " + self.exportValueToText(self.coatom)   + "\n"
        text += "atom =     " + self.exportValueToText(self.atom)     + "\n"
        text += "bottom =   " + self.exportValueToText(self.bottom)   + "\n"
        text += "cellSize = " + self.exportValueToText(self.cellSize)
        return text

    def exportToText(self):
        return self.exportDesignatedValuesToText()

    # ----------------------------------------------------------------------
    #
    #   Show
    #
    # ----------------------------------------------------------------------

    def show(self):
        """Prints the values of this object to the terminal."""
        print self.exportToText()

        """Prints the values of this object to the terminal."""
    def showDesignatedValues(self):
        print "size:", self.size
        print "top:", self.top
        print "coatom:", self.coatom
        print "atom:", self.atom
        print "bottom:", self.bottom

#ch = FiniteChain(-4,7)
#ch.show()
