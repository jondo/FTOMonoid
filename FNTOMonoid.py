import copy

from Error import *
from AlignText import *
from Counter import Counter
from FiniteChain import FiniteChain
from FTOMonoidMulTable import FTOMonoidMulTable
from FTOMonoidLevelEquivalence import FTOMonoidLevelEquivalence
from FTOMonoid import FTOMonoid

class FNTOMonoid(FTOMonoid):
    """Finite, negative, totally ordered monoid (tomonoid)."""

    def __init__(self, counter = None, size=1):
        """Initializetion of the object.

            Parameters:
                'size' ... size of the finite negative tomonoid; the bottome
                    element will have the value 1-'size' and the top element will
                    have the value '0' (same as the neutral element)
        """
        FTOMonoid.__init__(self, counter, 1 - size, 0)
        self.impossibleCoExtensions = set([])

    # -------------------------------------------------------------------------
    #
    #   Methods to clone the instance
    #
    # -------------------------------------------------------------------------

    def getCopy(self):
        """Returns the (deep) copy of the object."""
        new = FNTOMonoid(self.counter)
        new.createAsCopy(self)
        return new

    def getBottomDoublingExtension(self):
        """Returns the bottom-doubling extension of the tomonoid.

            All the pairs that are supposed to belong to the bottom-class or to
                the atom-class, are "undefined".
        """
        new = FNTOMonoid(self.counter)
        new.createAsBottomDoublingExtension(self)
        return new

    def getMaximalOneElementCoExtension(self):
        """Returns the maximal one element co-extension of the tomonoid.

            All the pairs that would be "undefined" in the bottom-doubling
                extension are related with the atom level set class.
        """
        new = FNTOMonoid(self.counter)
        new.createAsMaximalOneElementCoExtension(self)
        return new

    # -------------------------------------------------------------------------
    #
    #   Methods to obtain informations about the tomonoid
    #
    # -------------------------------------------------------------------------

    def getIdempotents(self):
        """Returns all the idempotents except for bottom and top elements."""
        idempotents = []
        for i in range(self.bottom + 1, self.top, 1):
            if self.getValue((i, i)) == i:
                idempotents.append(i)
        return idempotents

    def getBorderIdempotents(self):
        """Returns the pair of the border-defining idempotents.

            It is the pair of the idempotents with respect to which this
                tomonoid has been created as a co-extension.

            Returns:
                pair (tuple) of the tomonoid values
        """
        idemLeft = self.bottom
        while self.getValue((idemLeft, self.atom)) == self.bottom:
            idemLeft += 1
        idemRight = self.bottom
        while self.getValue((self.atom, idemRight)) == self.bottom:
            idemRight += 1
        return (idemLeft, idemRight)

    # -------------------------------------------------------------------------
    #
    #   "create as" methods
    #
    # -------------------------------------------------------------------------

    def createAsBottomDoublingExtension(self, orig):
        """Creates this instance as the bottom-doubling extension.

            Creates this instance of the class 'FNTOMonoid' as the
                bottom-doubling extension of the tomonoid.
            All the pairs that are supposed to belong to the bottom-class or to
                the atom-class, are "undefined".

            Parameters:
                'orig' ... an instance of the type FTOMonoid
        """
        # setting the new bottom to be under the original bottom
        self.setDesignatedValues(orig.bottom - 1, orig.top)

        # creating multiplication table as empty
        self.tab.setDesignatedValues(orig.bottom - 1, orig.top)
        self.tab.createEmptyMulTable()

        # creating level set equivalence as a copy of the original one
        self.leq = orig.leq.getCopy()
        self.leq.setDesignatedValues(orig.bottom - 1, orig.top)

        # removing the bottom-class from the level set equivalence
        self.leq.removeLvlSet(orig.bottom)

        # transcripting the (non-bottom, non-atom) values of the pairs in the
        # level set equivalence to the multiplication table
        lvlSetKeys = self.leq.getLvlSetKeys()
        for lvlSetKey in lvlSetKeys:
            lvlSet = self.leq.getLvlSet(lvlSetKey)
            for pair in lvlSet:
                self.tab.setValue(pair, lvlSetKey)

        # adding empty level set classes for the new bottom and atom
        self.leq.addLvlSet(self.atom)
        self.leq.addLvlSet(self.bottom)

        # adding the necessary pairs to the atom class and adding the
        # corresponding values to the multiplication table
        for pair in [(0, self.atom), (self.atom, 0)]:
            self.leq.addToLvlSet(pair, self.atom)
            self.tab.setValue(pair, self.atom)

        # adding the necessary pairs to the bottom class and adding the
        # corresponding values to the multiplication table
        for pair in [(0, self.bottom), (self.bottom, 0)]:
            self.leq.addToLvlSet(pair, self.bottom)
            self.tab.setValue(pair, self.bottom)
        self.leq.createBottomPairs()
        self.tab.setValuesOnBottom()

        # adding the "undefined" pairs both to the level set equivalence and to
        # the multiplication table
        for pair in orig.leq.getLvlSet(orig.bottom):
            if pair != (0, orig.bottom) and pair != (orig.bottom, 0):
                lvlSetKey = self.leq.addPairToNewUndefinedClass(pair)
                self.tab.setValue(pair, lvlSetKey)

        ## family relations
        #self.setParent(orig)
        #orig.setChild(self)

    def createAsMaximalOneElementCoExtension(self, orig):
        """Creates this instance as the maximal one element co-extension.

            Creates this instance of the class 'FNTOMonoid' as the maximal one
                element co-extension of the tomonoid.
            All the pairs that would be "undefined" in the bottom-doubling
                extension are related with the atom level set class.
            
            Parameters:
                'orig' ... an instance of the type FTOMonoid
        """

        self.createAsCopy(orig)

        # setting the new bottom to be under the original bottom
        self.setDesignatedValues(orig.bottom - 1, orig.top)
        self.leq.setDesignatedValues(orig.bottom - 1, orig.top)
        self.tab.setDesignatedValues(orig.bottom - 1, orig.top)

        # creating multiplication table as empty
        self.tab.setDesignatedValues(orig.bottom - 1, orig.top)
        self.tab.createEmptyMulTable()

        # adding empty level set class for the new bottom
        self.leq.addLvlSet(self.bottom)

        # adding the necessary pairs to the bottom class and adding the
        # corresponding values to the multiplication table
        for pair in [(0, self.bottom), (self.bottom, 0)]:
            self.leq.addToLvlSet(pair, self.bottom)
        self.leq.createBottomPairs()

        # all the pairs should be "defined" at this moment

        # transcripting the values of the pairs in the level set equivalence to
        # the multiplication table
        lvlSetKeys = self.leq.getLvlSetKeys()
        for lvlSetKey in lvlSetKeys:
            lvlSet = self.leq.getLvlSet(lvlSetKey)
            for pair in lvlSet:
                self.tab.setValue(pair, lvlSetKey)

    def createAsMinimum(self):
        """Creates the minimum finite negative tomonoid.
            
            The monoidal operation is given by x * y = min {x, y}.
        """
        self.tab.createAsMaximal()
        self.leq.createAsMaximal()

    # -------------------------------------------------------------------------
    #
    #   Methods to define or change the level set equivalence
    #
    # -------------------------------------------------------------------------

    def setPairToBottom(self, pair):
        """Sets the value of pair to bottom.
       
            The value of the given pair is set to 'bottom' both in the
                multiplication table and in the level set equivalence.
            The level set equivalence is taken into account; all the pairs that
                are in the same class are set to 'bottom', as well.
            The monotonicity is taken into account; all the pairs that are
                below the given pair (i.e., both of their coordinates are less or
                equal to the given pair coordinates) are set to 'bottom', as well.

            If any of the pairs, that is supposed to be set to 'bottom',
                already has a defined value distinct from 'bottom',
                ErrorMultipleValues is raised.

            Parameters:
                'pair' ... pair (tuple) of integers
        """
        lvlSetKey = self.tab.getValue(pair)
        self.setLvlSetToBottom(lvlSetKey)

    def setLvlSetToBottom(self, lvlSetKey):
        """Merges the given "undefined" level set with the bottom level set.
       
            The monotonicity is taken into account.

            If any of the pairs, that is supposed to be set to 'bottom',
                already has a defined value distinct from 'bottom',
                ErrorMultipleValues is raised.

            Parameters:
                'lvlSetKey' ... index of the level set
        """
        if self.atom <= lvlSetKey <= self.top:
            raise ErrorMultipleValues, ([self.bottom, lvlSetKey], self)
        elif lvlSetKey != self.bottom:
            lvlSet = self.leq.getLvlSet(lvlSetKey)
            copyOfLvlSet = copy.copy(lvlSet)
            self.leq.mergeLvlSets(lvlSetKey, self.bottom)
            for pair in copyOfLvlSet:
                self.tab.setValue(pair, self.bottom)
            for pair in copyOfLvlSet:
                x = pair[0]
                y = pair[1]
                if x > self.atom:
                    self.setPairToBottom((x - 1, y))
                if y > self.atom:
                    self.setPairToBottom((x, y - 1))

    def setPairToAtom(self, pair):
        """Sets the value of pair to atom.
       
            The value of the given pair is set to 'atom' both in the
                multiplication table and in the level set equivalence.
            The level set equivalence is taken into account; all the pairs that
                are in the same class are set to 'atom', as well.
            The monotonicity is taken into account; all the pairs that are
                "undefined" and above the given pair (i.e., both of their
                coordinates are greater or equal to the given pair coordinates) are
                set to 'atom', as well.

            If any of the pairs, that is supposed to be set to 'atom',
                already has a defined value distinct from 'atom',
                ErrorMultipleValues is raised.

            Parameters:
                'pair' ... pair (tuple) of integers
        """
        lvlSetKey = self.tab.getValue(pair)
        self.setLvlSetToAtom(lvlSetKey)

    def setLvlSetToAtom(self, lvlSetKey):
        """Merges the given "undefined" level set with the atom level set.
       
            The monotonicity is taken into account.

            If any of the pairs, that is supposed to be set to 'bottom',
                already has a defined value distinct from 'bottom',
                ErrorMultipleValues is raised.

            Parameters:
                'lvlSetKey' ... index of the level set
        """
        if self.atom < lvlSetKey <= self.top or lvlSetKey == self.bottom:
            raise ErrorMultipleValues, ([self.atom, lvlSetKey], self)
        elif lvlSetKey != self.atom:
            lvlSet = self.leq.getLvlSet(lvlSetKey)
            copyOfLvlSet = copy.copy(lvlSet)
            self.leq.mergeLvlSets(lvlSetKey, self.atom)
            for pair in copyOfLvlSet:
                self.tab.setValue(pair, self.atom)
            for pair in copyOfLvlSet:
                x = pair[0]
                y = pair[1]
                if not self.bottom <= self.tab.getValue((x + 1, y)) <= self.top:
                    self.setPairToAtom((x + 1, y))
                if not self.bottom <= self.tab.getValue((x, y + 1)) <= self.top:
                    self.setPairToAtom((x, y + 1))

    def relatePairsInZ(self, pair1, pair2):
        """Relates the two given pairs.

            Parameters:
                pair1, pair2 ... two pairs (tuples) of integers (tomonoid
                    values)
        """
        lvlSetKey1 = self.tab.getValue(pair1)
        if self.atom < lvlSetKey1 <= self.top:
            raise Error, "Pair " + str(pair1) + " with value " + str(lvlSetKey1) + " is not in Z."
        lvlSetKey2 = self.tab.getValue(pair2)
        if self.atom < lvlSetKey2 <= self.top:
            raise Error, "Pair " + str(pair2) + " with value " + str(lvlSetKey2) + " is not in Z."

        if lvlSetKey1 == self.bottom:
            if lvlSetKey2 == self.bottom:
                pass
            elif lvlSetKey2 == self.atom:
                raise ErrorMultipleValues, ([lvlSetKey1, lvlSetKey2], self)
            else:
                self.setPairToBottom(pair2)
        elif lvlSetKey1 == self.atom:
            if lvlSetKey2 == self.bottom:
                raise ErrorMultipleValues, ([lvlSetKey1, lvlSetKey2], self)
            elif lvlSetKey2 == self.atom:
                pass
            else:
                self.setPairToAtom(pair2)
        else:
            if lvlSetKey2 == self.bottom:
                self.setPairToBottom(pair1)
            elif lvlSetKey2 == self.atom:
                self.setPairToAtom(pair1)
            else:
                self.setPairToAtom(pair2)

        lvlSetKey = self.leq.mergeLvlSets(lvlSetKey1, lvlSetKey2)
        lvlSet = self.leq.getLvlSet(lvlSetKey)
        for pair in lvlSet:
            self.tab.setValue(pair, lvlSetKey)

    # -------------------------------------------------------------------------
    #
    #   Computation
    #
    # -------------------------------------------------------------------------

    def getLeftNegation(self, i):
        """Computes the left negation of the given value.
        
            Parameters:
                'i' ... a tomonoid value

            Returns:
                the left negation of the parameter 'i'
        """
        if i == self.bottom:
            return 0
        else:
            j = self.bottom
            while self.getValue((i, j + 1)) == self.bottom:
                j += 1
            return j

    def getRightNegation(self, j):
        """Computes the right negation of the given value.
        
            Parameters:
                'j' ... a tomonoid value

            Returns:
                the left negation of the parameter 'j'
        """
        if j == self.bottom:
            return 0
        else:
            i = self.bottom
            while self.getValue((i + 1, j)) == self.bottom:
                i += 1
            return i

    # -------------------------------------------------------------------------
    #
    #   one element co-extensions
    #
    # -------------------------------------------------------------------------

    # ----------------------------------------------------------------------
    #   rules
    # ----------------------------------------------------------------------

    def performE2(self, idemLeft, idemRight):
        """Performs rule (E2)."""

        if self.getSize() > 2:
            # value d represents a level set higher than atom and less than the
            # neutral element 0
            for d in range(self.atom + 1, 0, 1):
                lvlSet = self.leq.getLvlSet(d)
                # take all the pairs (a,b) in the level set d
                for pair in lvlSet:
                    a = pair[0]
                    b = pair[1]
                    # take only those pairs (a,b) that do not belong to the unit axes
                    if a < 0 and b < 0:
                        # take all the coordinates c ...
                        for c in range(self.atom + 1, 0, 1):
                            # ... such that (b,c) does not belong to the area Z
                            e = self.getValue((b,c))
                            if self.atom < e < 0:
                                # relate (d,c) and (a,e)
                                self.relatePairs((d,c), (a,e))

    def getInitialE3aPair(self):
        """Iterates pairs to perform (E3a). 
        
            Returns the first pair of the iteration.

            Exclusively for usage in method 'performE3a()'.
        """
        return self.getNextE3aPair((self.bottom, -1))

    def getNextE3aPair(self, pair):
        """Iterates pairs to perform (E3a). 
        
            Returns the next pair of the iteration.

            Exclusively for usage in method 'performE3a()'.
        """
        i = pair[0]
        j = pair[1]

        while j >= self.bottom and self.atom < self.getValue((i + 1, j)) <= 0:
            j -= 1
        if j < self.bottom:
            return None
        while i < 0 and not (self.atom < self.getValue((i + 1, j)) <= 0):
            i += 1
        if i >= 0:
            return None

        return (i, j)

    def performE3aForOnePair(self, pair, idemLeft, idemRight):
        """Performs rule (E3a) for a single pair."""

        # (a,b) in Z, (b,c) ~ e, c < idemRight => (a,e) ~ 0
        a = pair[0]
        b = pair[1]
        c = idemRight - 1
        e = self.getValue((b, c))
        if self.bottom <= e <= 0:
            self.setPairToBottom((a, e))

        # (b,c) in Z, (a,b) ~ d, a < idemLeft => (d,c) ~ 0
        b = pair[0]
        c = pair[1]
        a = idemLeft - 1
        d = self.getValue((a, b))
        if self.bottom <= d <= 0:
            self.setPairToBottom((d, c))

    def performE3a(self, idemLeft, idemRight):
        """Performs rule (E3a)."""

        if self.getSize() > 2:
            pair = self.getInitialE3aPair()
            while pair != None:
                self.performE3aForOnePair(pair, idemLeft, idemRight)
                pair = self.getNextE3aPair(pair)

    def performE3bForOnePair(self, pair, idemLeft, idemRight):
        """Performs rule (E3b) for a single pair."""

        # (a,b) in Z, (b,c) ~ e, c = idemRight => (a,e) ~ (a,b)
        a = pair[0]
        b = pair[1]
        c = idemRight
        e = self.getValue((b, c))
        if self.bottom <= e <= 0:
            self.relatePairs((a, e), (a, b))

        # (b,c) in Z, (a,b) ~ d, a = idemLeft => (d,c) ~ (b,c)
        b = pair[0]
        c = pair[1]
        a = idemLeft
        d = self.getValue((a, b))
        if self.bottom <= d <= 0:
            self.relatePairs((d, c), (b, c))

    def performE3b(self, idemLeft, idemRight):
        """Performs rule (E3b)."""

        if self.getSize() > 2:
            # iterate through all the pairs (a,b) in Z such that a < idemLeft and b < idemRight
            for i in range(self.atom + 1, idemLeft, 1):
                j = self.atom + 1
                lvlSetKey = self.getValue((i, j))
                while j < idemRight and not (self.atom < lvlSetKey <= 0):
                    if lvlSetKey != self.bottom and lvlSetKey != self.atom:
                        self.performE3bForOnePair((i, j), idemLeft, idemRight)
                    j += 1
                    lvlSetKey = self.getValue((i, j))

    def performE3c(self, idemLeft, idemRight):
        """Performs rule (E3c)."""

        if self.getSize() > 2:
            # (a,b), (b,c) in Z, a < idemLeft, c >= idemRight => (a,b) ~ 0
            if idemRight < 0:
                a = idemLeft - 1
                c = idemRight
                b = self.atom
                while not (self.atom < self.getValue((b + 1, c)) <= 0):
                    b += 1
                self.setPairToBottom((a, b))

            # (a,b), (b,c) in Z, a >= idemLeft, c < idemRight => (b,c) ~ 0
            if idemLeft < 0:
                a = idemLeft
                c = idemRight - 1
                b = self.atom
                while not (self.atom < self.getValue((a, b + 1)) <= 0):
                    b += 1
                self.setPairToBottom((b, c))

    def performE4a(self, idemLeft, idemRight):
        """Performs rule (E4a)."""

        if self.getSize() > 2:
            for i in range(self.atom + 1, idemLeft, 1):
                self.setValue((i, self.atom), self.bottom)
            for i in range(self.atom + 1, idemRight, 1):
                self.setValue((self.atom, i), self.bottom)
            self.setValue((self.atom, self.atom), self.bottom)

    def performE4b(self, idemLeft, idemRight):
        """Performs rule (E4b)."""

        if self.getSize() > 2:
            for i in range(idemLeft, 0, 1):
                j = self.atom
                lvlSetKey = self.getValue((i, j))
                while not (self.atom < lvlSetKey <= 0):
                    self.setValue((i, j), self.atom)
                    j += 1
                    lvlSetKey = self.getValue((i, j))

            for j in range(idemRight, 0, 1):
                i = self.atom
                lvlSetKey = self.getValue((i, j))
                while not (self.atom < lvlSetKey <= 0):
                    self.setValue((i, j), self.atom)
                    i += 1
                    lvlSetKey = self.getValue((i, j))

    # ----------------------------------------------------------------------
    #   co-extensions
    # ----------------------------------------------------------------------

    def getRamification(self, idemLeft, idemRight):
        """Returns the ramification with respect to the given pair of idempotents."""

        if self.getValue((idemLeft,idemLeft)) != idemLeft:
            raise Error, "The value " + str(idemLeft) + " is not an idempotent!"
        if self.getValue((idemRight,idemRight)) != idemRight:
            raise Error, "The value " + str(idemRight) + " is not an idempotent!"

        ram = self.getBottomDoublingExtension()

        ram.performE2(idemLeft, idemRight)
        ram.performE4a(idemLeft, idemRight)
        ram.performE4b(idemLeft, idemRight)
        ram.performE3a(idemLeft, idemRight)
        ram.performE3b(idemLeft, idemRight)
        ram.performE3c(idemLeft, idemRight)

        return ram

    def getCoarsenings(self, ram):
        """Returns the coarsenings of the given ramification.

            Parameters:
                ram ... ramification - instance of FNTOMonoid on which the
                    rules E1-4 have been performed; some of the level set classes
                    may be "undefined"
            Returns:
                list of f.n.tomonoids (instances of FNTOMonoid) that are
                coarsenings of the given ramification
        """
        lvlSetKey = None
        for key in ram.leq.getLvlSetKeys():
            if not (ram.bottom <= key <= ram.top):
                lvlSetKey = key
                break
        if lvlSetKey == None:
            return [ram]
        else:
            setToBottom = ram
            #setToAtom = ram.getCopy(counter.getNew())
            setToAtom = ram.getCopy()
            setToBottom.setLvlSetToBottom(lvlSetKey)
            setToAtom.setLvlSetToAtom(lvlSetKey)
            coarseningsBottom = self.getCoarsenings(setToBottom)
            coarseningsAtom = self.getCoarsenings(setToAtom)
            coarseningsBottom.extend(coarseningsAtom)
            return coarseningsBottom

    def areThereAnyCoExtensions(self, idemLeft, idemRight):
        cond1 = idemRight - 1 <= self.getLeftNegation(self.getLeftNegation(idemLeft))
        cond2 = idemLeft  - 1 <= self.getRightNegation(self.getRightNegation(idemRight))
        return cond1 and cond2

    def getOneElementCoExtensionsWithRespectToGivenIdempotents(self, idemLeft, idemRight):
        """Returns the co-extensions of this f.n.tomonoid with respect to the given pair of idempotents.

            Parameters:
                idemLeft ... left idempotent (tomonoid value)
                idemRight ... left idempotent (tomonoid value)

            Returns:
                list of f.n.tomonoids (instances of FNTOMonoid)
        """
        global outFile
        try:
            ram = self.getRamification(idemLeft, idemRight)
            #if self.areThereAnyCoExtensions(idemLeft, idemRight):
            #    #print "1. ok"
            #    pass
            #else:
            #    print "1. !!! NOT OK !!! "
            return self.getCoarsenings(ram)
        except ErrorMultipleValues, e:
            #print "No co-extensions with respect to", (idemLeft, idemRight)
            #self.show()
            self.impossibleCoExtensions.add((idemLeft, idemRight))
            #self.impossibleCoExtensions.add((idemLeft, idemRight, self.areThereAnyCoExtensions(idemLeft, idemRight)))
            #if not self.areThereAnyCoExtensions(idemLeft, idemRight):
            #    print "2. ok"
            #    #pass
            #else:
            #    print "2. !!! NOT OK !!! "
            #    #print e.values
            #    #self.show()
            #    #self.saveConditionToTeXFile("ext. not possible, " + str((idemLeft, idemRight)), + ", " + str(e.values))
            #    self.saveConditionToTeXFile("ext. not possible, " + str((idemLeft, idemRight)))
            #    #print (idemLeft, idemRight)
            return []

    def getOneElementCoExtensions(self):
        """Returns all the co-extensions of this f.n.tomonoid.

            Returns:
                list of f.n.tomonoids (instances of FNTOMonoid)
        """
        if self.bottom == 0:
            coex = FNTOMonoid(self.counter, 2)
            coex.createAsMinimum()
            self.setChild(coex)
            coex.setParent(self)
            return [ coex ]
        else:
            # find the idempotents
            if self.bottom == 0:
                idempotents = [ 0 ]
            else:
                idempotents = []
                for i in range(0, self.bottom, -1):
                    if self.getValue((i, i)) == i:
                        idempotents.append(i)
            # find co-extensions with respect to every possible pair of idempotents
            coExtensions = []
            for idemLeft in idempotents:
                for idemRight in idempotents:
                    coExtensions.extend(self.getOneElementCoExtensionsWithRespectToGivenIdempotents(idemLeft, idemRight))
            coExtensions.append(self.getMaximalOneElementCoExtension())

            # set family relations
            self.setChild(coExtensions[0])
            previous = None
            for coex in coExtensions:
                if previous != None:
                    previous.setSibling(coex)
                coex.setParent(self)
                coex.setChild(None)
                coex.setSibling(None)
                previous = coex

            return coExtensions

    # ----------------------------------------------------------------------
    #
    #   Export
    #
    # ----------------------------------------------------------------------

    # ----------------------------------------------------------------------
    # exporting to text format
    # ----------------------------------------------------------------------

    def exportToText(self):
        """Exports the values of this object to text.
            
            Returns:
                string containing text representation of this object; it can be
                    printed by the 'print' command
        """
        text = ""
        text += "Finite negative tomonoid"
        if self.serialNumber != None:
            text += ", no. " + str(self.serialNumber)
        text += "\n"
        text += self.tab.exportMulTableToText()
        return text

    def exportAlgebraicPropertiesToText(self):
        """Exports algebraic properties of the multiplication table to text.

            Returns:
                string containing text representation of this object; it can be
                    printed by the 'print' command
        """
        text = ""
        text += "Associative: " + str(self.tab.isAssociative()) + "  " + str(self.tab.getAssociativityTestReport()) + "\n"
        text += "Commutative: " + str(self.tab.isCommutative()) + "  " + str(self.tab.getCommutativityTestReport()) + "\n"
        text += "Monotone:    " + str(self.tab.isMonotone())    + "  " + str(self.tab.getMonotonicityTestReport())  + "\n"
        text += "Idempotent:  " + str(self.tab.isNegIdempotent())                                               + "\n"
        text += "Archimedean: " + str(self.tab.isNegArchimedean())                                              + "\n"
        text += "Drastic:     " + str(self.tab.isNegDrastic())
        return text

    def exportShortDescriptionToText(self):
        text = ""
        text += str(self.getSerialNumber())
        text += "(" + str(self.getSize()) + "): "
        text += self.exportChildrenToText()
        return text

    # ----------------------------------------------------------------------
    # exporting to TeX format
    # ----------------------------------------------------------------------

    def exportImpossibleCoExtensionsToTeX(self):
        """Exports the values of the attribute 'impossibleCoExtensions' to TeX.
            
            Returns:
                string containing TeX source code
        """
        text = ""
        if self.impossibleCoExtensions:
            text += "No co-extensions for the pairs: "
            first = True
            for pair in sorted(self.impossibleCoExtensions):
                if first:
                    first = False
                else:
                    text += ", "
                text += "$" + str(pair) + "$"
            text += "\n"
        return text

    # ----------------------------------------------------------------------
    #
    #   Saving to a file
    #
    # ----------------------------------------------------------------------

    def saveConditionToTeXFile(self, comment):

        fileName = self.getTeXFilePath()

        out = open(fileName, "w")

        out.write(self.getTeXHead())
        out.write(self.exportMulTableToTeX())
        out.write("\\newline\n")
        out.write(comment)
        out.write(self.getTeXTail())


        out.close()
        print "File", fileName, "written."

    # ----------------------------------------------------------------------
    #
    #   Show
    #
    # ----------------------------------------------------------------------

    def show(self):
        print self.exportToText()

    def showAlgebraicProperties(self):
        print self.exportAlgebraicPropertiesToText()

    def showShortDescription(self):
        print self.exportShortDescriptionToText()

# **************************************************************************
#
#   Run
#
# **************************************************************************

tableArch =    [['-5','-4','-3','-2','-1',' 0'],
                ['-5','-5','-5','-4','-3','-1'],
                ['-5','-5','-5','-5','-5','-2'],
                ['-5','-5','-5','-5','-5','-3'],
                ['-5','-5','-5','-5','-5','-4'],
                ['-5','-5','-5','-5','-5','-5']]

tableNonArch = [['-8','-7','-6','-5','-4','-3','-2','-1',' 0'],
                ['-8','-8','-8','-5','-4','-3','-2','-1','-1'],
                ['-8','-8','-8','-5','-4','-4','-2','-2','-2'],
                ['-8','-8','-8','-8','-8','-8','-5','-3','-3'],
                ['-8','-8','-8','-8','-8','-8','-5','-4','-4'],
                ['-8','-8','-8','-8','-8','-8','-5','-5','-5'],
                ['-8','-8','-8','-8','-8','-8','-8','-8','-6'],
                ['-8','-8','-8','-8','-8','-8','-8','-8','-7'],
                ['-8','-8','-8','-8','-8','-8','-8','-8','-8']]

tableCntEx1 =  [['-3','-2','-1',' 0'],
                ['-3','-3','-1','-1'],
                ['-3','-3','-2','-2'],
                ['-3','-3','-3','-3']]

tableCntEx2 =  [['-6','-5','-4','-3','-2','-1',' 0'],
                ['-6','-5','-4','-3','-2','-1','-1'],
                ['-6','-6','-4','-4','-2','-2','-2'],
                ['-6','-6','-6','-5','-4','-3','-3'],
                ['-6','-6','-6','-6','-4','-4','-4'],
                ['-6','-6','-6','-6','-6','-5','-5'],
                ['-6','-6','-6','-6','-6','-6','-6']]


def testFNTOMonoid_getOneElementCoExtensions(table):
    print "***********************************************************************"
    ftom = FNTOMonoid(Counter())
    ftom.createFromTable(table)
    ftom.show()
    ftom.showFamilyRelations()
    ftom.showAlgebraicProperties()
    coExtensions = ftom.getOneElementCoExtensions()
    print "-----------------------------------------------------------------------"
    print "number of co-extensions:", len(coExtensions)
    print "-----------------------------------------------------------------------"
    for coex in coExtensions:
        coex.show()
        coex.showFamilyRelations()
        print "-----------------------------------------------------------------------"

def testFNTOMonoid_getMaximalOneElementCoExtension(table):
    print "***********************************************************************"
    ftom = FNTOMonoid(Counter())
    ftom.createFromTable(table)
    ftom.show()
    maximal = ftom.getMaximalOneElementCoExtension()
    print "-----------------------------------------------------------------------"
    maximal.show()

#testFNTOMonoid_getOneElementCoExtensions(tableArch)
#testFNTOMonoid_getMaximalOneElementCoExtension(tableArch)
#
#ftom = FNTOMonoid(Counter(), 5)
#ftom.createAsMinimum()
#ftom.show()
#ftom.showLevelSets()

#ftom = FNTOMonoid(Counter())
#ftom.createFromTable(tableArch)
#ftom.saveToTeXFile()

#ftom = FNTOMonoid(Counter())
#ftom.createFromTable(tableCntEx2)
#ftom.show()
#ftom.showAlgebraicProperties()
#coExtensions = ftom.getOneElementCoExtensions()
#print "num of co-extensions:", len(coExtensions)
#for coex in coExtensions:
#    coex.show()
#    print "border:", coex.getBorderIdempotents()
