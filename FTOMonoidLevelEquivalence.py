import copy

from Error import *
from AlignText import *
from FiniteChain import FiniteChain

class FTOMonoidLevelEquivalence(FiniteChain):
    """Level set equivalence of a finite totally ordered monoid (tomonoid)

        Attributes:
            'lvlSets' ...  associative array of sets of pairs (two element tuples): 
                Each set represents a level set of the monoidal multiplication. 
                A level set can be either "defined" or "undefined".
                If the level set is "defined" then the key of this set in the
                    associative array 'lvlSets' corresponds with the value to which
                    the pairs in this set are evaluated when speaking about the
                    monoidal multiplication.
                If the level set is "undefined" then it is not yet decided to
                    which monoid value the pairs in this level set will be
                    evaluated; in this case the key of the associative array will
                    be equal to a value that is out of the range of the monoid
                    values.
    """

    def __init__(self, bottom=0, top=0):
        """Sets designated values and creates an associative array with empty
            level sets.

            Creates an empty table (i.e. with undefined values) of the size
                given by the parameters 'bottom' and 'top'.

            Parameters:
                'bottom' ... the minimal value of the finite tomonoid
                'top' ... the maximal value of the finite tomonoid
        """
        FiniteChain.__init__(self, bottom, top)
        self.createEmptyLvlSets()
        self.allPairsDefined = False

    # -------------------------------------------------------------------------
    #
    #   Methods that initialize the object
    #
    # -------------------------------------------------------------------------

    def createEmptyLvlSets(self):
        """Creates an associative array with empty level sets.
        
            Each such level set will contain no pair.
        """
        self.lvlSets = {}
        for i in range(self.bottom, self.top + 1, 1):
            #self.lvlSets[i] = set([])
            self.addLvlSet(i)
        self.lastUndefinedClass = self.top

    def createUnitPairs(self):
        """Creates pairs with unit coordinate."""
        self.lvlSets[0].add((0,0))
        for i in range(self.bottom, self.top + 1, 1):
            if i != 0:
                self.lvlSets[i].add((i,0))
                self.lvlSets[i].add((0,i))

    def createBottomPairs(self):
        """Creates pairs with 'bottom' coordinate."""
        if self.bottom < 0:
            self.lvlSets[self.bottom].add((self.bottom,self.bottom))
            for i in range(self.bottom + 1, 0, 1):
                self.lvlSets[self.bottom].add((i,self.bottom))
                self.lvlSets[self.bottom].add((self.bottom,i))

    def createTopPairs(self):
        """Creates pairs with 'top' coordinate."""
        if self.top > 0:
            self.lvlSets[self.top].add((self.top,self.top))
            for i in range(self.top - 1, 0, -1):
                self.lvlSets[self.top].add((i,self.top))
                self.lvlSets[self.top].add((self.top,i))

    def getNewUndefinedClass(self):
        """Returns a new "undefined" level set class.

            Returns:
                key of the new "undefined" level set class
        """
        self.lastUndefinedClass += 1
        return self.lastUndefinedClass

    def addPairToNewUndefinedClass(self, pair):
        """Creates a new "undefined" level set class containing the given pair.

            Returns:
                key of the new "undefined" level set class
        """
        key = self.getNewUndefinedClass()
        self.lvlSets[key] = set([pair])
        return key

    def createUndefinedPairs(self):
        """Create the rest of the pairs as "undefined".

            I.e., those that have not been created by 'createUnitPairs',
                'createBottomPairs', and 'createTopPairs'.
        """
        # negative part
        for i in range(self.bottom + 1, 0, 1):
            for j in range(self.bottom + 1, 0, 1):
                self.addPairToNewUndefinedClass((i,j))
        # positive part
        for i in range(1, self.top, 1):
            for j in range(1, self.top, 1):
                self.addPairToNewUndefinedClass((i,j))
        # negative-positive  part
        for i in range(self.bottom, 0, 1):
            for j in range(1, self.top + 1, 1):
                self.addPairToNewUndefinedClass((i,j))
        # positive-negative part
        for i in range(1, self.top + 1, 1):
            for j in range(self.bottom, 0, 1):
                self.addPairToNewUndefinedClass((i,j))

    # -------------------------------------------------------------------------
    #
    #   Methods to work with level set classes contained in 'lvlSets'
    #
    # -------------------------------------------------------------------------

    def mergeLvlSets(self, key1, key2):
        """Merge two level set classes into one.
            
            The two classes will be deleted from 'lvlSets' and a new class,
                containing the pairs of both these classes, will be created.

            Parameters:
                key1 ... key (in 'lvlSets') of the first class
                key2 ... key (in 'lvlSets') of the second class

            Returns:
                key of the new resulting level set class
        """
        if key1 == None or key2 == None:
            raise Error, "Level set key equal to None!"

        if key1 == key2:
            return key1
        else:
            if self.bottom <= key2 <= self.top:
                if self.bottom <= key1 <= self.top:
                    raise ErrorMultipleValues, ([key1, key2], self)
                else:
                    self.lvlSets[key2].update(self.lvlSets[key1])
                    #self.lvlSets.remove(self.lvlSets[key1])
                    #del self.lvlSets[key1]
                    self.removeLvlSet(key1)
                    return key2
            else:
                self.lvlSets[key1].update(self.lvlSets[key2])
                #self.lvlSets.remove(self.lvlSets[key2])
                #del self.lvlSets[key2]
                self.removeLvlSet(key2)
                return key1

    def getLvlSetKeys(self):
        return self.lvlSets.keys()

    def getLvlSet(self, key):
        return self.lvlSets[key]

    def findLvlSet(self, pair):
        """Finds the level set that contains the given pair.

                This method is highly ineffective and serves for debugging
                    purposes only.

            Parameters:
                pair ... pair (tuple) of tomonoid values

            Returns:
                the value (level set class key) to which the given pair belongs;
                if the pair is not found the 'None' is returned
        """
        for key in self.lvlSets.keys():
            if pair in self.lvlSets[key]:
                return key
        #raise Error, "Level equivalence class not found!"
        return None

    def addToLvlSet(self, pair, lvlSetKey):
        """Adds a pair to a level set.

            Parameters:
                pair ... pair (tuple) of tomonoid values
                lvlSetKey ... index of the level set in 'lvlSets'
        """
        if self.lvlSets.has_key(lvlSetKey):
            self.lvlSets[lvlSetKey].add(pair)
        else:
            raise Error, "Level equivalence class " + str(lvlSetKey) + " not found!"

    def removeFromLvlSet(self, pair, lvlSetKey):
        """Removes a pair from a level set.

            Parameters:
                pair ... pair (tuple) of tomonoid values
                lvlSetKey ... index of the level set in 'lvlSets'
        """
        if self.lvlSets.has_key(lvlSetKey):
            self.lvlSets[lvlSetKey].remove(pair)
        else:
            raise Error, "Level equivalence class " + str(lvlSetKey) + " not found!"

    def addLvlSet(self, lvlSetKey):
        """Adds a new empty level set class.

            Parameters:
                lvlSetKey ... index of the level set in 'lvlSets'
        """
        self.lvlSets[lvlSetKey] = set([])

    def removeLvlSet(self, lvlSetKey):
        """Removes a whole level set class.

            Parameters:
                lvlSetKey ... index of the level set in 'lvlSets'
        """
        if self.lvlSets.has_key(lvlSetKey):
            del self.lvlSets[lvlSetKey]
        else:
            raise Error, "Level equivalence class " + str(lvlSetKey) + " not found!"

    def isInLvlSet(self, pair, lvlSetKey):
        """Tests whether a pair is contained in a level set.

            Parameters:
                pair ... pair (tuple) of tomonoid values
                lvlSetKey ... index of the level set in 'lvlSets'

            Returns:
                'True' ... if 'pair' is contained in the level set specified by 'lvlSetKey'
                'False' ... otherwise
        """
        if self.lvlSets.has_key(lvlSetKey):
            return pair in self.lvlSets[lvlSetKey]
        else:
            raise Error, "Level equivalence class " + str(lvlSetKey) + " not found!"

    def isTomonoidValue(self, value):
        """Tests whether the given value is within the range of the tomonoid.

            Parameters:
                'value' ... an integer

            Returns:
                'True' ... if 'value' is within the range of the tomonoid,
                    i.e., if 'bottom' <= 'value' <= 'top'
        """
        return self.bottom <= value <= self.top

    # -------------------------------------------------------------------------
    #
    #   Methods to clone multiplication table and to compare two multiplication
    #   tables
    #
    # -------------------------------------------------------------------------

    def getCopy(self):
        """Returns the (deep) copy of the object."""
        new = FTOMonoidLevelEquivalence()
        new.createAsCopy(self)
        return new

    # -------------------------------------------------------------------------
    #
    #   "create as" methods
    #   -------------------
    #
    #   These methods serve to fill all the values in the multiplication
    #   table to obtain various kinds of special types of tomonoids.
    #
    # -------------------------------------------------------------------------

    def createAsCopy(self, orig):
        """Creates this level set equivalence as the (deep) copy of 'orig'.

            Parameters:
                'orig' ... an instance of the type FTOMonoidLevelEquivalence
        """
        self.setDesignatedValues(orig.bottom, orig.top)
        self.lvlSets = copy.deepcopy(orig.lvlSets)

    def createAsUndefined(self):
        """Creates this level set equivalence as undefined.

            Only the pairs, whose monoid multiplication value is given by the
                theory, are defined.
            These are the pairs with unit coordinate and the pairs with a
                coordinate equal to 'bottom' or 'top' and contained, respectively,
                in the negative or positive cone.
            The rest of the pairs is "undefined".
        """
        self.createEmptyLvlSets()
        self.createUnitPairs()
        self.createBottomPairs()
        self.createTopPairs()
        self.createUndefinedPairs()

    def createAsMinimal(self):
        """Creates this level set equivalence to represent the minimal tomonoid.

            The monoidal multiplication on the positive part is the maximum, on
                the negative part is equal to 'bottom', and on the side part it is
                the minimum.
        """
        pass

    def createAsMaximal(self):
        """Creates this level set equivalence to represent the minimal tomonoid.

            The monoidal multiplication on the positive part is equal to 'top', on the
                negative part it is the minimum, and on the side part it is the
                maximum.
        """
        self.createUnitPairs()

        for i in range(self.bottom, 0):
            for j in range(self.bottom, 0):
                lvlSetKey = min(i,j)
                self.lvlSets[lvlSetKey].add((i,j))

        for i in range(1, self.top + 1):
            for j in range(1, self.top + 1):
                self.lvlSets[self.top].add((i,j))

        for i in range(self.bottom, 0):
            for j in range(1, self.top + 1):
                self.lvlSets[j].add((i,j))
                self.lvlSets[j].add((j,i))

        self.allPairsDefined = True

    def createAsTrivial(self):
        """Creates this level set equivalence such that it represents the trivial monoid.

            The trivial monoid is the monoid that contains the neutral element
                '0' only.
            Therefore the resulting level set equivalence will have only one
                class containing the pair (0,0).
        """
        self.setDesignatedValues(0, 0)
        self.createEmptyLvlSets()
        self.addToLvlSet((0,0), 0)
        self.allPairsDefined = True

    def createFromTable(self, table):
        """Creates this level set equivalence according to the given table of values.
            
            Parameters:
                'table' ... list of n lists of n values from 'bottom', ..., '0', ..., 'top'

            Example:
                table= [['-2','-1',' 2',' 2',' 2'],
                        ['-2','-1',' 1',' 2',' 2'],
                        ['-2','-1',' 0',' 1',' 2'],
                        ['-2','-2','-1','-1','-1'],
                        ['-2','-2','-2','-2','-2']]
                leq = FTOMonoidLevelEquivalence()
                leq.createFromTable(table)
        """

        # size of the multiplication table
        self.size = len(table)

        # looking for the neutral element
        unit = None
        for x in range(self.size):
            if int(table[self.size - x - 1][x]) == 0:
                unit = x
                break
        if unit == None:
            raise Error, "Unit element not found in the given table!"

        # setting the designated values
        self.setDesignatedValues(-unit, self.size - unit - 1)

        # creating the multiplication table
        self.createEmptyLvlSets()

        # filling the multiplication table
        for x in range(self.size):
            for y in range(self.size):
                value = int(table[y][self.size - 1 - x])
                i = (self.size - y - 1) + self.bottom
                j = (self.size - x - 1) + self.bottom
                self.addToLvlSet((i,j), value)

    # ----------------------------------------------------------------------
    #
    #   Export
    #
    # ----------------------------------------------------------------------

    # ----------------------------------------------------------------------
    # exporting to text format
    # ----------------------------------------------------------------------

    def exportClassesToText(self):
        """Exports the level equivalence classes (level sets) to text.

            Returns:
                string containing the formated table; can be printed by the
                    'print' command
        """
        text = ""
        firstA = True
        keys = self.lvlSets.keys()
        keys.sort(reverse=True)
        for key in keys:
            if firstA:
                firstA = False
            else:
                text += "\n"
            text += alignInteger(key, self.cellSize)
            text += ": { "
            firstB = True
            for pair in self.lvlSets[key]:
                if firstB:
                    firstB = False
                else:
                    text += ", "
                text += str(pair)
            text += " }"
        return text

    def exportToText(self):
        """Exports the values of this object to text.
            
            Returns:
                string containing text representation of this object; it can be
                    printed by the 'print' command
        """
        text = ""
        text += self.exportClassesToText()
        return text

    def show(self):
        """Prints the values of this object to the terminal."""
        print self.exportToText()

## **************************************************************************
##
##   Run
##
## **************************************************************************
#
#def testFTOMonoidLevelEquivalence_1():
#    table= [['-2','-1',' 2',' 2',' 2'],
#            ['-2','-1',' 1',' 1',' 2'],
#            ['-2','-1',' 0',' 1',' 2'],
#            ['-2','-2','-1','-1','-1'],
#            ['-2','-2','-2','-2','-2']]
#    leq = FTOMonoidLevelEquivalence()
#    leq.createFromTable(table)
#    leq.show()
#
#testFTOMonoidLevelEquivalence_1()
#
#
#
