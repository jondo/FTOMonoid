
class Error(Exception):
    """A general error.
    The only attribute is 'text' with a description of the error."""

    def __init__(self, text):
        """
        Parameters:
            'text' ... the description of the error
        """
        self.text = text

    def __str__(self):
        return self.text

class ErrorMultipleValues(Error):
    """Error which is referring to the fact, that there are assigned multiple
        values to one level equivalence class of a tomonoid."""

    def __init__(self, values, tomonoid):
        """
        Parameters:
            'values' ... list of the values that are assigned to one level
                equivalence class of a tomonoid
        """
        self.values = values
        self.tomonoid = tomonoid
        self.text = "Attempt to assign multiple values to a single level set class: " + str(self.values)
        #+ " on tomonoid of size " + str(self.tomonoid.size) + "\n" + self.tomonoid.exportTableToText() + "\n" + self.tomonoid.parent.exportTableToText()

